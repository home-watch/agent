use std::time::Duration;

use actix::{Actor, AsyncContext, AtomicResponse, Context, Handler, Running, WrapFuture, ActorFuture};

use crate::agent::messages::{RegisterMsg, PingMsg};
use crate::client::HttpClient;
use crate::client::models;
use crate::config::Configuration;
use std::sync::Arc;
use crate::client::models::{System, SystemParams};

mod messages;

pub use messages::ShutdownMsg;
use std::env::consts::{OS, ARCH};

pub struct Agent {
    name: String,
    ping_interval: Duration,
    client: Arc<HttpClient>,
    sys: Option<System>,
}

impl Agent {
    pub fn new(cfg: &Configuration) -> Agent {
        let agent_config = cfg.agent();
        let client = HttpClient::new(agent_config.server_url(), agent_config.token());
        Agent {
            name: cfg.system_name(),
            ping_interval: agent_config.ping_interval(),
            client: Arc::new(client),
            sys: None,
        }
    }

    fn sys_or_panic(&self) -> System {
        if let Some(sys) = self.sys.clone() {
            sys
        } else {
            log::error!("no system found, maybe something went wrong during registration");
            actix::System::current().stop();
            panic!()
        }
    }
}

impl Actor for Agent {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::info!("Agent started");

        ctx.notify(RegisterMsg { sys: models::SystemParams::base(self.name.clone()) });
    }

    fn stopping(&mut self, _ctx: &mut Self::Context) -> Running {
        log::info!("Agent stopping...");
        Running::Stop
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        log::info!("Agent stopped");
    }
}

impl Handler<RegisterMsg> for Agent {
    type Result = AtomicResponse<Self, ()>;

    fn handle(&mut self, msg: RegisterMsg, _: &mut Self::Context) -> Self::Result {
        log::debug!("registering system");
        let client = self.client.clone();

        AtomicResponse::new(Box::pin(
            async move { client.register(msg.sys).await }
                .into_actor(self)
                .map(|res, this, ctx| match res {
                    Ok(sys) => {
                        log::info!("registered system {}", sys.name);
                        let mut sys = sys.clone();
                        sys.operating_system = Some(format!("{} {}", OS, ARCH));
                        this.sys = Some(sys);
                        ctx.run_interval(this.ping_interval, move |_act, ctx| {
                            ctx.notify(PingMsg)
                        });
                    }
                    Err(err) => {
                        log::error!("{}", err);
                        actix::System::current().stop();
                    }
                })
        ))
    }
}

impl Handler<PingMsg> for Agent {
    type Result = AtomicResponse<Self, ()>;

    fn handle(&mut self, _: PingMsg, _ctx: &mut Self::Context) -> Self::Result {
        log::debug!("sending ping");
        let client = self.client.clone();
        let sys = self.sys_or_panic();

        AtomicResponse::new(Box::pin(async move {
            let params = SystemParams::from_system(&sys);
            client.ping(sys.id, params).await
        }
            .into_actor(self)
            .map(|res, _this, _ctx| match res {
                Ok(()) => {
                    log::debug!("ping successful");
                }
                Err(err) => {
                    log::error!("{}", err);
                }
            })
        ))
    }
}

impl Handler<ShutdownMsg> for Agent {
    type Result = AtomicResponse<Self, ()>;

    fn handle(&mut self, _: ShutdownMsg, _ctx: &mut Self::Context) -> Self::Result {
        log::debug!("sending offline status");
        let client = self.client.clone();
        let sys = self.sys_or_panic();

        AtomicResponse::new(Box::pin(async move { client.offline(sys.id).await }
            .into_actor(self)
            .map(|res, _this, _ctx| match res {
                Ok(()) => {
                    log::debug!("system successfully marked as offline");
                }
                Err(err) => {
                    log::error!("{}", err);
                }
            })
        ))
    }
}