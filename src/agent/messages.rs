use actix::Message;
use crate::client::models::SystemParams;

pub struct RegisterMsg {
    pub sys: SystemParams,
}

impl Message for RegisterMsg {
    type Result = ();
}

pub struct PingMsg;

impl Message for PingMsg {
    type Result = ();
}

pub struct ShutdownMsg;

impl Message for ShutdownMsg {
    type Result = ();
}