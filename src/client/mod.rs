use reqwest::{Client, Method};
use url::Url;
use crate::client::models::{SystemParams, System};
use crate::client::error::Error;

pub mod error;
pub mod models;

pub type Result<T> = std::result::Result<T, Error>;

pub struct HttpClient {
    base_url: Url,
    token: String,
    client: Client,
}

impl HttpClient {
    pub fn new(base_url: Url, token: String) -> HttpClient {
        let client = Client::builder()
            .use_rustls_tls()
            .build().unwrap();
        HttpClient {
            base_url,
            token,
            client,
        }
    }

    pub async fn register(&self, sys: SystemParams) -> Result<System> {
        let url = self.base_url.join("/api/v1/systems")?;
        self.client.request(Method::POST, url)
            .bearer_auth(self.token.as_str())
            .json(&sys)
            .send().await?
            .error_for_status()?
            .json().await
            .map_err(Error::from)
    }

    pub async fn ping(&self, id: String, sys: SystemParams) -> Result<()> {
        let path = format!("/api/v1/systems/{}", id.to_string());
        let url = self.base_url.join(path.as_str())?;
        self.client.request(Method::PUT, url)
            .bearer_auth(self.token.as_str())
            .json(&sys)
            .send().await?
            .error_for_status()
            .map(|_| {()})
            .map_err(Error::from)
    }

    pub async fn offline(&self, id: String) -> Result<()> {
        let path = format!("/api/v1/systems/{}/offline", id.to_string());
        let url = self.base_url.join(path.as_str())?;
        self.client.request(Method::POST, url)
            .bearer_auth(self.token.as_str())
            .send().await?
            .error_for_status()
            .map(|_| {()})
            .map_err(Error::from)
    }
}