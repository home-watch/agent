#[derive(Debug, Clone, Default, Eq, PartialEq, Deserialize, Serialize)]
pub struct System {
    pub id: String,
    pub name: String,
    pub ipv4: Option<String>,
    pub ipv6: Option<String>,
    pub operating_system: Option<String>,
    pub status: Status,
}

#[derive(Debug, Default, Eq, PartialEq, Deserialize, Serialize)]
pub struct SystemParams {
    pub name: Option<String>,
    pub ipv4: Option<String>,
    pub ipv6: Option<String>,
    pub operating_system: Option<String>,
}

impl SystemParams {
    pub fn base(name: String) -> SystemParams {
        SystemParams { name: Some(name), ..Default::default() }
    }

    pub fn from_system(sys: &System) -> SystemParams {
        let System {
            name,
            ipv4,
            ipv6,
            operating_system,
            ..
        } = sys.clone();
        SystemParams {
            ipv4,
            ipv6,
            name: Some(name),
            operating_system,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum Status {
    Unknown,
    Pending,
    Active,
    Warning,
    Offline,
}

impl Default for Status {
    fn default() -> Self {
        Status::Unknown
    }
}