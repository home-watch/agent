use std::{fmt, error};

#[derive(Debug)]
pub enum Error {
    HttpErr(reqwest::Error),
    GenericErr(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::HttpErr(req_err) => req_err.fmt(f),
            Error::GenericErr(s) => write!(f, "{}", s),
        }
    }
}

// This is important for other errors to wrap this one.
impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::HttpErr(req_err) => Some(req_err),
            _ => None
        }
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Self {
        Error::HttpErr(err)
    }
}

impl From<url::ParseError> for Error {
    fn from(err: url::ParseError) -> Self {
        Error::GenericErr(err.to_string())
    }
}