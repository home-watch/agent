use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "homewatchd", about = "Homewatch agent")]
pub struct Opts {
    /// Specify a config file
    // short and long flags (-d, --debug) will be deduced from the field's name
    #[structopt(short, long, default_value = "/etc/homewatch/config.toml")]
    pub config: PathBuf,
}