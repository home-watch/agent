#[macro_use]
extern crate serde_derive;

use std::io;
use crate::agent::{Agent, ShutdownMsg};
use actix::{Actor, Addr};
use structopt::StructOpt;
use crate::config::Configuration;
use crate::cli::Opts;

mod agent;
mod cli;
mod client;
mod config;

async fn handle_shutdown(agent: Addr<Agent>) {
    tokio::signal::ctrl_c().await.unwrap();
    log::warn!("Ctrl-C received, shutting down");
    agent.send(ShutdownMsg).await.unwrap();
    actix::System::current().stop();
}

fn main() -> io::Result<()> {
    let opts = Opts::from_args();
    let cfg = Configuration::from_opts(opts).unwrap();
    let lvl = cfg.log_level();
    std::env::set_var("RUST_LOG", format!("error,homewatchd={}", lvl.to_string()));
    // std::env::set_var("RUST_LOG", lvl.to_string());

    env_logger::init();

    log::info!("Starting Homewatch agent");

    let system = actix::System::new("agent");

    let agent = Agent::new(&cfg);
    let agent_addr = agent.start();

    actix::spawn(handle_shutdown(agent_addr));

    system.run()
}