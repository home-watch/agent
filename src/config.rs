use config::{Config, File, ConfigError};
use std::str::FromStr;
use crate::cli::Opts;
use url::Url;
use std::time::Duration;

#[derive(Debug, Deserialize)]
pub struct Configuration {
    log: Log,
    agent: Agent,
    system: System,
}

#[derive(Debug, Deserialize)]
struct Log {
    level: String,
}

#[derive(Debug, Deserialize)]
pub struct Agent {
    server_url: String,
    ping_interval: String,
    token: String,
}

#[derive(Debug, Deserialize)]
pub struct System {
    name: String,
}

impl Configuration {
    pub fn from_opts(opts: Opts) -> Result<Self, ConfigError> {
        let mut c = Config::new();

        c.merge(File::from(opts.config))?;

        c.set_default("log.level", "info")?;

        c.try_into()
    }

    pub fn log_level(&self) -> log::Level {
        let lvl = self.log.level.as_str();
        log::Level::from_str(lvl).expect(format!("invalid log level: '{}'", lvl).as_str())
    }

    pub fn system_name(&self) -> String {
        self.system.name.clone()
    }

    pub fn agent(&self) -> &Agent {
        &self.agent
    }
}

impl Agent {
    pub fn server_url(&self) -> Url {
        self.server_url.parse().expect("invalid server url")
    }

    pub fn ping_interval(&self) -> Duration {
        self.ping_interval.parse::<humantime::Duration>().expect("invalid interval expression").into()
    }

    pub fn token(&self) -> String {
        self.token.clone()
    }
}
