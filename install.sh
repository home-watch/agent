#!/usr/bin/env sh

cargo build --release

sudo cp target/release/homewatchd /usr/bin/homewatchd
sudo cp init/systemd/homewatchd.service /usr/lib/systemd/system/homewatchd.service